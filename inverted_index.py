import regex as re
import string
import math
import operator
import codecs

class InvertedIndex:
    def __init__(self):
        self.index = {}
        self.document_word_count = {}

        #Regex used to find terms.
        self.word_regex = re.compile(r"[\w'-åäöÅÄÖ]+", re.UNICODE)
        self.tf_idf_vector = {}

    def word_is_valid(self, word):
        return word.isnumeric() == False
    
    def process_file(self, path, filename):
        '''Parses the input file and inserts the terms and their postitions into the inverted index'''
        assert(filename not in self.document_word_count.keys())
        file_offset = 0
        word_count = 0
        with codecs.open(path + "/" + filename, encoding='ansi') as fp:
            for line in fp:
                line = line.lower()

                #Parse the words
                words = self.word_regex.findall(line)

                #Remove any special characters
                words = [re.sub(r"[^\w\s]",'', w) for w in words]

                #Remove duplicate terms by converting the list to a set
                words = set([w for w in words if self.word_is_valid(w)])
                for word in words:
                    if self.word_is_valid(word):

                        #Find the postions of the word in the line and add the relative file offset.
                        pos = [file_offset + m.start() for m in re.finditer(r'\b({})\b'.format(word), line)]
                        word_count += len(pos)
                        if word not in self.index.keys():
                            self.index[word] = {}
                        if filename not in self.index[word].keys():
                            self.index[word][filename] = []
                        self.index[word][filename].extend(pos)
                file_offset += len(line)
        self.document_word_count[filename] = word_count
    
    def compute_tf_idf(self):
        '''Computes the tf-idf values'''
        for term in self.index.keys():
            idf = math.log10(len(self.document_word_count.keys()) / len(self.index[term].keys()))

            for doc in self.index[term].keys():
                tf = len(self.index[term][doc]) / self.document_word_count[doc]
        
                if doc not in self.tf_idf_vector.keys():
                    self.tf_idf_vector[doc] = []
                tf_idf = tf * idf

                #Store the term and its tf-idf score as a tuple in the document's vector
                self.tf_idf_vector[doc].append((term, tf_idf))

    def display_index(self):
        print(self.index)
        print("\n")
    
    def find_word(self, word):
        return self.index[word]
    
    def display_tf_idf_vector(self):
        for doc in self.tf_idf_vector.keys():
            print(doc + " : ")
            print(sorted(self.tf_idf_vector[doc], key=operator.itemgetter(1), reverse=True))
            print("\n")
    
    def output_to_file(self):
        '''Writes the inverted index and the tf-idf vector to files'''
        with codecs.open("index.txt", "w", encoding="ansi") as fp:
            fp.write(str(self.index))
        
        with codecs.open("tfidf_vector.txt", "w", encoding="ansi") as fp:
            for doc in self.tf_idf_vector.keys():
                fp.write(doc + " : \n")

                #Sort the vector based on the tf-idf value before writing to file
                fp.write(str(sorted(self.tf_idf_vector[doc], key=operator.itemgetter(1), reverse=True)))
                fp.write("\n")
