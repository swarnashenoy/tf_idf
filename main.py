import os
from inverted_index import InvertedIndex

def main():
    i = InvertedIndex()

    data_path = "data"
    for filename in os.listdir(data_path):
        if filename.endswith(".txt"):
            i.process_file(data_path, filename)
    i.compute_tf_idf()
    #i.display_index()
    #i.display_tf_idf_vector()
    i.output_to_file()

if __name__ == "__main__":
    main()
